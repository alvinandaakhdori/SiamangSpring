package com.keldua.siamangspring.model;

public class Product {

  private String id;
  private String title;
  private String imagePath;
  private int price;

  public Product() {}

  public Product(String id, String title, String imagePath, int price) {
    super();
    this.id = id;
    this.title = title;
    this.imagePath = imagePath;
    this.price = price;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }
}
