package com.keldua.siamangspring.service;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.keldua.siamangspring.controller.ProductController;
import com.keldua.siamangspring.model.Product;

@Service
public class ProductService {

  private List<Product> products =
      new ArrayList<>(
          Arrays.asList(
              new Product(
                  "historical-romance",
                  "Historical Romance: The Earl's Mistaken Bride (Mempelai Sang Earl)",
                  "https://cdn.gramedia.com/uploads/items/9786020382340_Historical-Ro__w200_hauto.jpg",
                  70000),
              new Product(
                  "chicklit",
                  "ChickLit: Katwalk",
                  "https://cdn.gramedia.com/uploads/items/9786020378275_ChickLit-Katw__w200_hauto.jpg",
                  85000),
              new Product(
                  "dragon-ball",
                  "Dragon Ball Vol. 29",
                  "https://cdn.gramedia.com/uploads/items/9786020459776_Dragon_Ball_29__w200_hauto.jpg",
                  25000)));

  public List<Product> getAllProducts() {
    return products;
  }

  public Product getProduct(String id) {
    return products.stream().filter(p -> p.getId().equals(id)).findFirst().get();
  }

  public void addProduct(Product product) {
    products.add(product);
  }

  public void updateProduct(String id, Product product) {
    for (int i = 0; i < products.size(); i++) {
      Product p = products.get(i);
      if (p.getId().equals(id)) {
        products.set(i, product);
        return;
      }
    }
  }

  public void deleteProduct(String id) {
    products.removeIf(p -> p.getId().equals(id));
  }
}
